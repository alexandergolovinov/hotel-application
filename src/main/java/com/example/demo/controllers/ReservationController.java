package com.example.demo.controllers;

import com.example.demo.entity.RoomReservation;
import com.example.demo.services.ReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/reservations")
public class ReservationController {
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final Logger LOG = LoggerFactory.getLogger(ReservationController.class);

    @Autowired
    private ReservationService reservationService;

    @RequestMapping(method = RequestMethod.GET)
    public String getReservations(@RequestParam(value = "date", required = false) String dateString, Model model) {
        Date date = null;
        if(!StringUtils.isEmpty(dateString)) {
            try {
                date = DATE_FORMAT.parse(dateString);
            } catch (ParseException e) {
                LOG.error("Error caught {}", e);
            }
        } else {
            date = new Date();
        }
        List<RoomReservation> roomReservationsList = reservationService.getRoomReservationsForDate(date);
        model.addAttribute("roomReservations", roomReservationsList);
        return "reservations";
    }

}
